FROM nginx:alpine
COPY ./bulka_tv/index.html /usr/share/nginx/html/index.html

COPY ./bulka_tv/css /usr/share/nginx/html/css
COPY ./bulka_tv/webfonts /usr/share/nginx/html/webfonts
COPY ./bulka_tv/img /usr/share/nginx/html/img